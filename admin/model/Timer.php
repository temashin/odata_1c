<?php


class Timer
{

	var $T1 = null;
	var $T2 = null;

	function __construct() {
		$this->start();
	}

	//запустили секундомер
	public function start() {
		$this->T1 = microtime(TRUE);
		$this->T2 = null;
	}

	//остановили секундомер
	public function stop() {
		$this->T2 = microtime(TRUE);
	}

	//измерить результат в секундах (вплоть до мкс)
	public function result() {
		//финишная отметка не определена
		if (is_null($this->T2)) {
			$this->stop();
		}
		return $this->T2 - $this->T1;
	}
}
