<?php
ini_set('memory_limit', '-1');

class ModelExtensionOdata extends Model
{
	/**
	 * @param $data object
	 * @return mixed id записанной категории
	 */
	public function addCategory($data)
	{
		$sql = "INSERT INTO " . DB_PREFIX . "category SET
		  odata_parent_id = '" . $this->db->escape($data->getParentId()) . "',
		  odata_id = '" . $this->db->escape($data->getOdataId()) . "', 
		  status = '0', 
		  date_modified = NOW(), 
		  date_added = NOW() 
		  ON DUPLICATE KEY UPDATE 
		  odata_id='" . $this->db->escape($data->getOdataId()) . "', 
		  odata_parent_id = '" . $this->db->escape($data->getParentId()) . "'";
		//запись в бд категории
		$this->db->query($sql);
		//возвращаем записанную категорию
		$category_id = $this->db->query("SELECT category_id FROM " . DB_PREFIX . "category WHERE odata_id = '" . $data->getOdataId() . "'");
		$category_id = $category_id->row['category_id'];

		$name = $data->getName();
		$sql2 = "INSERT INTO " . DB_PREFIX . "category_description SET 
		category_id = '" . (int)$category_id . "', 
		language_id = '1', 
		name = '" . $this->db->escape($name) . "', 
		description = '" . $this->db->escape($name) . "' 
		ON DUPLICATE KEY UPDATE 
		category_id = '" . (int)$category_id . "',
		name = '" . $this->db->escape($name) . "',
		description = '" . $this->db->escape($name) . "'";

		//описание категориии
		$this->db->query($sql2);
		//вернуть категорию для записи

		$this->db->query("INSERT INTO " . DB_PREFIX . "category_to_store SET 
		category_id='" . $category_id . "',
		store_id='0' ON DUPLICATE KEY UPDATE 
		category_id='" . $category_id . "'
		");

		return $category_id;

	}

	/**
	 * @param $data object
	 */
	public function addProduct($data)
	{
		$sql_product = "INSERT INTO " . DB_PREFIX . "product SET
		sku = '" . $this->db->escape($data->getSku()) . "',
		quantity = '" . (int)$this->db->escape($data->getQuantity()) . "',
		parent_id ='" . $this->db->escape($data->getParentId()) . "',
		odata_id ='" . $this->db->escape($data->getOdataId()) . "',
		subtract = '1', stock_status_id = '7',
		shipping = '1', price = '0',
		status ='1',
		date_added = NOW() ON DUPLICATE KEY UPDATE
		odata_id='" . $this->db->escape($data->getOdataId()) . "',
		sku = '" . $this->db->escape($data->getSku()) . "',
		quantity = '" . (int)$this->db->escape($data->getQuantity()) . "',
		parent_id ='" . $this->db->escape($data->getParentId()) . "'";

		$this->db->query($sql_product);

		$product_id = $this->db->query("SELECT product_id FROM " . DB_PREFIX . "product WHERE odata_id ='" . $this->db->escape($data->getOdataId()) . "'");
		$product_id = $product_id->row['product_id'];

		$this->db->query("INSERT INTO " . DB_PREFIX . "product_description SET
		product_id='" . (int)$product_id . "',
		language_id='1',
		name='" . $this->db->escape($data->getName()) . "' ON DUPLICATE KEY UPDATE
		 product_id='" . (int)$product_id . "',
		 name='" . $this->db->escape($data->getName()) . "'");

		$this->db->query("INSERT INTO " . DB_PREFIX . "product_to_store SET 
		product_id ='" . $product_id . "',
		store_id = '0' ON DUPLICATE KEY UPDATE 
		product_id ='" . $product_id . "'");

	}

	/**
	 * записывает ценовые категории
	 * @param $data array
	 */
	public function addPriseCategory($data)
	{
		foreach ($data as $priceGroup) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "price_group SET 
			description = '" . $this->db->escape($priceGroup['Description']) . "',
			ref_key = '" . $this->db->escape($priceGroup['Ref_Key']) . "' 
			ON DUPLICATE KEY UPDATE 
				description = '" . $this->db->escape($priceGroup['Description']) . "',
			 ref_key = '" . $this->db->escape($priceGroup['Ref_Key']) . "' 
			");
		}
	}

	public function sortCategories()
	{

		$categories = $this->db->query("SELECT * FROM " . DB_PREFIX . "category WHERE 
		odata_parent_id  <> '00000000-0000-0000-0000-000000000000'");

		foreach ($categories->rows as $row) {

			$this->db->query("INSERT INTO " . DB_PREFIX . "category_path SET category_id = '" . $row['category_id'] . "', path_id = '" . $row['category_id'] . "', level='0' ON DUPLICATE KEY UPDATE   category_id = '" . $row['category_id'] . "', path_id = '" . $row['category_id'] . "'");

			$select = $this->db->query("SELECT category_id FROM " . DB_PREFIX . "category  WHERE odata_id = '" . $row['odata_parent_id'] . "' LIMIT 1");
			if (isset($select->row['category_id'])) {
				$this->db->query("UPDATE " . DB_PREFIX . "category c SET parent_id = {$select->row['category_id']}  WHERE c.category_id = " . $row['category_id']);
			}

		}

	}

	public function sortProducts()
	{

		$products = $this->db->query("SELECT parent_id, odata_id, product_id FROM " . DB_PREFIX . "product");

		$categories = [];

		$this->db->query("TRUNCATE `oc_product_to_category`");

		foreach ($products->rows as $product) {


			$category = $this->getCategoryId($product['parent_id']);

			if (empty($category)) {
				continue;
			}
			$categories[] = $category['category_id'];

			while ($category['odata_parent_id'] !== '00000000-0000-0000-0000-000000000000') {
				$category = $this->getCategoryId($category['odata_parent_id']);
				if (empty($category['category_id'])) {
					break;
				}
				$categories[] = $category['category_id'];
			}

			$this->addProductToCategory($product['product_id'], $categories);
			unset($categories);
		}


	}

	public function addProductToCategory($product_id, $categories_id = [])
	{

		foreach ($categories_id as $id) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "product_to_category SET
								product_id = '" . (int)$product_id . "',
								category_id = '" . $id . "',
								main_category = '1'
								ON DUPLICATE KEY UPDATE
								 product_id = '" . (int)$product_id . "',
								 category_id = '" . $id . "'");
		}
	}


	public function getCategoryId($id)
	{

		$category = $this->db->query("SELECT * FROM " . DB_PREFIX . "category WHERE odata_id = '" . $id . "'");

		return $category->row;
	}

	public function addCustomers($customers)
	{

		foreach ($customers as $customer) {
			//если не покупатель то преходим на следующую итерацию
			if (!$customer->isCustomer) {
				continue;
			}

			$user = $this->db->query("SELECT ref_key FROM " . DB_PREFIX . "customer where ref_key = '" . $customer->getRefKey() . "' LIMIT 1");
			//если есть то дальше
			if ($user->num_rows !== 0) {
				continue;
			}

			$randome_pass = $this->pass();
			$mail = "@mail.ru";
			$this->db->query("INSERT INTO " . DB_PREFIX . "customer SET ref_key ='" . $customer->getRefKey() . "',
			 salt = '" . $this->db->escape($salt = token(9)) . "',
			 password = '" . $this->db->escape(sha1($salt . sha1($salt . sha1($randome_pass)))) . "', email = '" . $randome_pass . $mail . "',
			 firstname = '" . $this->db->escape($customer->getName()) . "',
			 status = '1',
			 approved = '1',
			 customer_group_id = '1',
			 name_store = '" . $this->db->escape($customer->getFullName()) . "',
			 date_added = NOW()");

			$file = DIR_LOGS . 'customer.log';
			$tmp = fopen($file, 'a+');
			fwrite($tmp, $customer->getFullName() . ' - ' . $randome_pass . '-' . $randome_pass . $mail . "\r\n");
			fclose($tmp);
		}

	}

	/**
	 * table oc_document_set_prise_product
	 * @param $data array
	 */
	public function addSetDocumentPricesProduct($data)
	{
		foreach ($data as $priceProduct) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "document_set_prise_product SET 
		ref_key = '" . $this->db->escape($priceProduct['ref_key']) . "',
		product_кey = '" . $this->db->escape($priceProduct['product_кey']) . "',
		type_price_key = '" . $this->db->escape($priceProduct['type_price_key']) . "',
		price = " . $this->db->escape($priceProduct['price']) . ",  
		added = '" . $this->db->escape($priceProduct['date']) . "' ");
		}
	}

	public function addPricesProductType($data)
	{
		foreach ($data as $priceProduct) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "price_product_type SET 
				ref_key = '" . $this->db->escape($priceProduct['Ref_Key']) . "',
				description = '" . $this->db->escape($priceProduct['Description']) . "'
			");
		}
	}

	/**
	 * table oc_prices_product_type_by_customer_price_group
	 * @param $data
	 */
	public function addPricesProductTypeByCustomerPriceGroup($data)
	{
		foreach ($data as $priceGroup) {
			$this->db->query(
				"INSERT INTO " . DB_PREFIX . "prices_product_type_by_customer_price_group SET 
				ref_key = '" . $this->db->escape($priceGroup['Ref_Key']) . "',
				price_product_group = '" . $this->db->escape($priceGroup['НоменклатурнаяЦеноваяГруппа']) . "', 
				type_price = '" . $this->db->escape($priceGroup['ТипЦен_Key']) . "'"
			);
		}
	}

	/**
	 * table oc_prices_product_type_by_customer
	 * @param $data
	 */
	public function addPricesProductTypeByCustomer($data)
	{
		foreach ($data as $priceGroup) {
			$this->db->query(
				"INSERT INTO " . DB_PREFIX . "prices_product_type_by_customer SET 
				ref_key = '" . $this->db->escape($priceGroup['Ref_Key']) . "',
				customer_ref_key = '" . $this->db->escape($priceGroup['Контрагент_Key']) . "'
				");
		}
	}

	/**
	 * table oc_document_set_prise_product_type
	 * @param $data array
	 */
	public function addSetDocumentPricesProductType($data)
	{
		foreach ($data as $priceProduct) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "document_set_prise_product_type SET 
		ref_key = '" . $this->db->escape($priceProduct['Ref_Key']) . "',
		type_price_key = '" . $this->db->escape($priceProduct['ТипЦен_Key']) . "' 
			");
		}
	}


	/**
	 * Устанавливаем типы данных пользователя
	 * @param $data array
	 */
	public function addCustomersInformationType($data)
	{
		foreach ($data as $info) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "information_type SET
				ref_key = '" . $this->db->escape($info->getRefKey()) . "',
				description = '" . $this->db->escape($info->getDescription()) . "',
				type = '" . $this->db->escape($info->getType()) . "', 
				type_obj = '" . $this->db->escape($info->getTypeObj()) . "'
				ON DUPLICATE KEY UPDATE 
				ref_key = '" . $this->db->escape($info->getRefKey()) . "',
				description = '" . $this->db->escape($info->getDescription()) . "'
				");
		}
	}

	/**
	 * table oc_customer_info
	 * подгружаем информацию по пользователям
	 * @param $data array
	 */
	public function addCustomerInformation($data)
	{
		foreach ($data as $info) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "customer_info 
			SET 
			customer_ref_key = '" . $this->db->escape($info->getCustomerRefKey()) . "',
			type = '" . $this->db->escape($info->getType()) . "',
			type_ref_key = '" . $this->db->escape($info->getTypeRefKey()) . "',
			description = '" . $this->db->escape($info->getDescription()) . "' 
			ON DUPLICATE KEY UPDATE 
				customer_ref_key = '" . $this->db->escape($info->getCustomerRefKey()) . "',
			type = '" . $this->db->escape($info->getType()) . "',
			type_ref_key = '" . $this->db->escape($info->getTypeRefKey()) . "',
			description = '" . $this->db->escape($info->getDescription()) . "' 
			");
		}
	}

	/**
	 * курс валют
	 * @param $cys валюта
	 */
	public function addСurrency($cys)
	{
		foreach ($cys as $cy){
			$this->db->query("UPDATE `oc_currency` set value =".$cy['curs']." where currency_id =".$cy['currency_id']."");
		}
	}




	/**
	 *пишем в рабочую таблицу
	 */
	public function addProductToCustomer($id)
	{

		$priceToCustomer = $this->db->query("SELECT c.customer_id,c.ref_key as c_customer_ref_key, dsp.price, dsp.added, p.odata_id, p.product_id FROM `oc_customer` as c
			LEFT JOIN `oc_prices_product_type_by_customer` as tpp ON tpp.customer_ref_key = c.ref_key
			LEFT JOIN `oc_prices_product_type_by_customer_price_group` as tppg ON tppg.ref_key = tpp.ref_key
			LEFT JOIN `oc_document_set_prise_product` as dsp ON dsp.type_price_key = tppg.type_price
			LEFT JOIN `oc_product` as p ON p.odata_id = dsp.product_кey WHERE c.customer_id = " . $id . "");

		if (empty($priceToCustomer->rows[0]['price'])){
			return true;
		}
		$sql = "INSERT INTO `oc_tmp_price` (`price`,`added`, `customer_id`, `odata_customer_id`, `product_id`, `odata_product_id`) VALUES ";
		foreach ($priceToCustomer->rows as $customer) {
			if (empty($customer['price'])) {
				continue;
			}
			$sql .= "(" . $customer['price'] . ", '" .$customer['added']. "', ". $customer['customer_id'] . ", '" . $customer['c_customer_ref_key'] . "', " . $customer['product_id'] . ", '" . $customer['odata_id'] . "'), ";
		}

		$this->db->query(substr($sql, 0, -2));
	}

	public function priceToCat($prodId){
		$cat = $this->db->query("SELECT category_id  FROM `oc_product_to_category` where product_id = ".$prodId);
		return $cat->row['category_id'];
	}

	public function addActiveCustomers()
	{
		$activeCustomers = $this->db->query("SELECT DISTINCT (c.customer_id) FROM `oc_customer` as c 
		LEFT JOIN `oc_prices_product_type_by_customer` as tpp ON tpp.customer_ref_key = c.ref_key GROUP by tpp.customer_ref_key");

		$sql = "INSERT INTO `oc_customer_broker` (`id_customer`, `status`) VALUES ";

		foreach ($activeCustomers->rows as $customer) {
			$sql .= "(" . $customer['customer_id'] . ", 'none'), ";
		}

		$this->db->query(substr($sql, 0, -2));
	}

	public function getActiveCustomers()
	{
		$customers = $this->db->query("SELECT id_customer FROM `oc_customer_broker` ");

		return $customers->rows;
	}

	public function finaly()
	{
		//перезаписываем основнубю
		$this->db->query("DROP TABLE 	`oc_price_to_customer`");

		$this->db->query("CREATE TABLE `oc_price_to_customer` SELECT * FROM `oc_tmp_price`");
		$this->db->query("ALTER TABLE `oc_price_to_customer` ADD INDEX customer_id (customer_id)");
		$this->db->query("ALTER TABLE `oc_price_to_customer` ADD INDEX product_id (product_id);");
		//чистим временные таблицы
		$this->db->query("TRUNCATE `oc_prices_product_type_by_customer`");
		$this->db->query("TRUNCATE `oc_document_set_prise_product`");
		$this->db->query("TRUNCATE `oc_document_set_prise_product_type`");
		$this->db->query("TRUNCATE `oc_prices_product_type_by_customer_price_group`");
		$this->db->query("TRUNCATE `oc_price_group`");
		$this->db->query("TRUNCATE `oc_price_product_type`");
		$this->db->query("TRUNCATE `oc_price_group`");
		$this->db->query("TRUNCATE `oc_tmp_price`");
		$this->db->query("TRUNCATE `oc_broker`");
		$this->db->query("TRUNCATE `oc_customer_broker`");
	}

	private function pass()
	{
		$chars = "qazxswedcvfrtgbnhyujmkiolp1234567890QAZXSWEDCVFRTGBNHYUJMKIOLP";
		$max = 10;
		$size = StrLen($chars) - 1;
		$password = null;
		while ($max--) {
			$password .= $chars[rand(0, $size)];
		}

		return $password;

	}

	/**
	 *возващает время последнего изменения статуса
	 */
	public function watcher()
	{
		 $time = $this->db->query("SELECT start FROM `oc_progress_broker`");

		 return $time->row['start'];
	}
	/**
	 * выполняемый метод
	 * @param $method
	 *  выполнения скрипта в млс
	 * @param $time
	 * статус done или none
	 * @param $status
	 */
	public function setStatus($method, $time, $status)
	{
		$this->db->query("INSERT INTO `oc_broker` SET 
		method = '" . $method . "' ,
		status = '" . $status . "' ,
		time  = '" . $time . "'");
	}

	/**
	 * @param $method
	 * @return mixed
	 */
	public function getStatusByMethod($method)
	{
		$result = $this->db->query("SELECT * FROM `oc_broker` WHERE method = '" . $method . "'");

		return $result->row;
	}

	/**
	 * возвращает послденю запись в таблице
	 * @return mixed
	 */
	public function getLastMethod()
	{
		$method = $this->db->query("SELECT method FROM `oc_broker` ORDER BY id DESC LIMIT 1");

		return (!empty($method->row['method'])) ? $method->row['method'] : 'none';
	}

	public function updateStatus()
	{
		//$this->db->query("UPDATE `oc_broker` SET  WHERE ");
	}

	/**
	 * @return Timer
	 */
	public function timer()
	{
		spl_autoload_register(function ($class){
			include 'Timer.php';
		});

		return new Timer();
	}

	public function setProgress($status)
	{
		$status = ($status === 1) ? 1 : 0;
		$this->db->query("UPDATE `oc_progress_broker` SET in_progress = " . $status . ", start = NOW()");
	}

	public function getProgress()
	{
		$progress = $this->db->query("SELECT * FROM `oc_progress_broker`");

		return $progress->row['in_progress'];
	}


}
