<?php

use Kily\Tools1C\OData\Client;

require_once(DIR_PROJECT. '/vendor/autoload.php');

class ControllerExtensionOdata extends Controller
{
	public $client;

	public function index()
	{
		$client = new Client('http://'.IP.'/'.BASE.'/odata/standard.odata/', [
			'auth' => [
				LOGIN,
				PASS
			],
			'timeout' => 300,
		]);

		$this->client = $client;
		return $this->client;
	}

}
