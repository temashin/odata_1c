
<?php


class InformationType
{
		protected $refKey;

		protected $description;

		protected $type;

		protected $typeObj;

	/**
	 * InformationType constructor.
	 * Типы Описания Контрагентов
	 * Уникальный Ключ Ref_Key
	 * @param $refKey
	 * Description
	 * @param $description
	 * Тип
	 * @param $type
	 * ВидОбъектаКонтактнойИнформации
	 * @param $typeObj
	 */
	public function __construct($info)
		{
			$this->refKey = $info['Ref_Key'];
			$this->description = $info['Description'];
			$this->type = $info['Тип'];
			$this->typeObj = $info['ВидОбъектаКонтактнойИнформации'];
		}

	public function getRefKey()
	{
		return $this->refKey;
	}

	public function getDescription()
	{
		return $this->description;
	}

	public function getType()
	{
		return $this->type;
	}

	public function getTypeObj()
	{
		return $this->typeObj;
	}
}
