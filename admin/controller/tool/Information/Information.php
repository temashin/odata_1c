<?php


class Information
{
	protected $customerRefKey;
	protected $type;
	protected $typeRefKey;
	protected $description;


	public function __construct($param)
	{
		$this->customerRefKey = $param['Объект'];
		$this->type = $param['Тип'];
		$this->typeRefKey = $param['Вид'];
		$this->description = $param['Представление'];
	}

	/**
	 * @return mixed
	 */
	public function getCustomerRefKey()
	{
		return $this->customerRefKey;
	}

	/**
	 * @return mixed
	 */
	public function getType()
	{
		return $this->type;
	}

	/**
	 * @return mixed
	 */
	public function getTypeRefKey()
	{
		return $this->typeRefKey;
	}

	/**
	 * @return mixed
	 */
	public function getDescription()
	{
		return $this->description;
	}

}
