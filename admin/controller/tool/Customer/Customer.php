<?php


class Customer
{

	protected $name;

	/**
	 * содержит инн юзера
	 * @var mixed
	 */
	protected $inn;


	/**
	 * содержит кпп юзера
	 * @var mixed
	 */
	protected  $kpp;

	protected  $fullName;

	protected $refKey;

	/**
	 * ОсновнойДоговорКонтрагента_Key
	 * @var mixed
	 */
	protected $contract;

	/**
	 * покупатель
	 * @var bool
	 */
	public $isCustomer;


	public function __construct($param)
	{
		$this->name = $param['Description'];
		$this->inn = $param['ИНН'];
		$this->kpp = $param['КПП'];
		$this->fullName = $param['НаименованиеПолное'];
		$this->refKey = $param['Ref_Key'];
		$this->contract = $param['ОсновнойДоговорКонтрагента_Key'];
		$this->isCustomer = $param['Покупатель'];
	}

	/**
	 * @return mixed
	 */
	public function getContract()
	{
		return $this->contract;
	}

	/**
	 * @return mixed
	 */
	public function getRefKey()
	{
		return $this->refKey;
	}

	/**
	 * @return mixed
	 */
	public function getFullName()
	{
		return $this->fullName;
	}

	/**
	 * @return mixed
	 */
	public function getKpp()
	{
		return $this->kpp;
	}

	/**
	 * @return mixed
	 */
	public function getName()
	{
		return $this->name;
	}

}
