<?php

class ProductOdata extends  Product
{
	/**
	 * id из 1с
	 * @var
	 */
	protected $odata_id;

	/**
	 * id родительской категории
	 * @var
	 */
	protected $parent_id;

	/**
	 * является категорией
	 * @var bool
	 */
	public $isCategory;

	public function __construct($name, $description, $quantity, $sku, $odata_id, $parent_id,$is_category)
	{
		parent::__construct($name, $description, $quantity, $sku);
		$this->odata_id = $odata_id;
		$this->parent_id= $parent_id;
		$this->is_category = $is_category;
	}

	/**
	 * @param mixed $odata_id
	 */
	public function setOdataId($odata_id)
	{
		$this->odata_id = $odata_id;
	}

	/**
	 * @return mixed
	 */
	public function getOdataId()
	{
		return $this->odata_id;
	}

	/**
	 * @return mixed
	 */
	public function getParentId()
	{
		return $this->parent_id;
	}

	/**
	 * @param mixed $parent_id
	 */
	public function setParentId($parent_id)
	{
		$this->parent_id = $parent_id;
	}

}
