<?php
class Product
{

	/**
	 * содержит название имени
	 * @var string
	 */
	protected $name;

	/**
	 * содежит массив с описанием товара
	 * @var array
	 */
	protected $description;

	/**
	 * содержит количество товара
	 * @var
	 */
	protected $quantity;

	/**
	 * содержит артикул
	 * @var
	 */
	protected  $sku;

	/**
	 * @return mixed
	 */

	public function __construct($name, $description, $quantity, $sku )
	{
		$this->name = $name;
		$this->description = $description;
		$this->quantity = $quantity;
		$this->sku = $sku;
	}

	public function getName()
	{
		return $this->name;
	}

	/**
	 * @param mixed $name
	 */
	public function setName($name)
	{
		$this->name = $name;
	}

	/**
	 * @param array $description
	 * @return InstanceProductController
	 */
	public function setDescription($description)
	{
		$this->description = $description;
	}

	/**
	 * @return mixed
	 */
	public function getDescription()
	{
		return $this->description;
	}

	/**
	 * @param mixed $quantity
	 * @return InstanceProductController
	 */
	public function setQuantity($quantity)
	{
		$this->quantity = $quantity;
	}

	/**
	 * @return mixed
	 */
	public function getQuantity()
	{
		return $this->quantity;
	}

	/**
	 * @param mixed $sku
	 * @return InstanceProductController
	 */
	public function setSku($sku)
	{
		$this->sku = $sku;

	}

	/**
	 * @return mixed
	 */
	public function getSku()
	{
		return $this->sku;
	}

}
