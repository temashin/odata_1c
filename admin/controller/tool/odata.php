<?php

use Kily\Tools1C\OData\Client;

require_once(DIR_PROJECT . '/vendor/autoload.php');

class ControllerToolOdata extends Controller
{
	protected $odata;

	/**
	 * курсы валют
	 * @var array
	 */
	public $currency = [];

	public function index()
	{
		$this->load->model('extension/odata');

		if($this->model_extension_odata->getProgress() == 1){
			return true;
		}
		$this->model_extension_odata->setProgress(1);
		if(!$this->getOdata()){
			return false;
		};

		//получаем послдений метод
		$lastMethod = $this->model_extension_odata->getLastMethod();
		$timer = $this->model_extension_odata->timer();
		switch ($lastMethod){
			case 'none':
				$this->SetInformation();
				$this->model_extension_odata->setStatus('setInformation', $timer->result(), 'done');
				break;
			case 'setInformation':
				$this->sortProductsToCategory();
				$this->model_extension_odata->setStatus('sortProductsToCategory', $timer->result(), 'done');
				break;
			case 'sortProductsToCategory':
				$this->addActiveCustomers();
				$this->model_extension_odata->setStatus('addActiveCustomers', $timer->result(), 'done');
				break;
			case 'addActiveCustomers':
			$customers = $this->model_extension_odata->getActiveCustomers();

			foreach ($customers as $customer){
				$this-> sortCustomers($customer['id_customer']);
				$this-> model_extension_odata->setStatus('addActiveCustomers', $timer->result(), 'done');
			}
			$this->model_extension_odata->setStatus('finaly', $timer->result(), 'done');
			break;

			case 'finaly':
				$this->model_extension_odata->finaly();
			break;
		}
		$this->model_extension_odata->setProgress(0);
		echo 'done';
	}

	/**
	 *запускаем раз в 20 минут
	 * если статус начала и конца отличаются то запускаем
	 */
	public function watcher()
	{
		$this->load->model('extension/odata');
		$time = $this->model_extension_odata->watcher();

		if (60 - date_diff(new DateTime(), new DateTime($time))->i > 20) {
			$this->model_extension_odata->setProgress(0);
		}else {
			return true;
		}
	}

	/**
	 *уставнавливаем сооединени с odata
	 */
	public function  getOdata(){
		//подключение файлов
		spl_autoload_register(function ($class){
			include_once 'Product/Product.php';
			include_once 'Product/ProductOdata.php';
			include_once 'Customer/Customer.php';
			include_once 'Information/Information.php';
			include_once 'Information/InformationType.php';
		});
		ini_set('max_execution_time', 0);
		$this->odata = $this->load->controller('extension/odata');
		try{
			$test = $this->odata->{'Catalog_Номенклатура/?$select=Ref_Key&$top=1&'}->get()->values();
			if(!$test){
				throw new Exception('{"error" : 500}');
			}
		}catch (Exception $ex){
			echo $ex->getMessage();
			$this->model_extension_odata->setProgress(0);
			return  false;
		}
		return true;
	}


	public function clear()
	{
		$this->load->model('extension/odata');
		$this->model_extension_odata->finaly();
		$this->model_extension_odata->setProgress(0);
		echo 'done';
	}

	protected  function   setInformation()
	{

		$odata_result = $this->getCatalog();
		$result_quantity = $this->getQuantyty();

		foreach ($odata_result as $prod) {

			$quantity = !empty($result_quantity[$prod['Ref_Key']]) ? $result_quantity[$prod['Ref_Key']] : 0;

			$article = !empty($prod['Артикул']) ? $prod['Артикул'] : '0';

			$products_odata[] = new ProductOdata($prod['Description'], $prod['Description'], $quantity, $article, $prod['Ref_Key'], $prod['Parent_Key'],$prod['IsFolder']);

		};
		//		запись в бд
		foreach ($products_odata as $product_odata) {
			if ($product_odata->is_category) {
				$this->model_extension_odata->addCategory($product_odata);
			} else {
				$this->model_extension_odata->addProduct($product_odata);
			}
		}
		//курсы валют
		$this->getCurrency();
		//$this->model_extension_odata->addСurrency($this->currency);

		$this->model_extension_odata->addPricesProductTypeByCustomerPriceGroup($this->getPricesProductTypeByCustomerPriceGroup());
		//отношение ценовой группы к контрагенту
		$this->model_extension_odata->addPricesProductTypeByCustomer($this->getPricesProductTypeByCustomer());
		//ценовые группы
		$this->model_extension_odata->addPricesProductType($this->getPricesProductType());
		//добавляем категории цен
		$this->model_extension_odata->addPriseCategory($this->getPrisesCategory());
		//Добавлянм поьзователей
		$this->model_extension_odata->addCustomers($this->getCustomers());
		//Добавляем Информацию о пользователях
		$this->model_extension_odata->addCustomerInformation($this->getCustomerInformation());
		//Добавляем типы информации о пользователях
		$this->model_extension_odata->addCustomersInformationType($this->getCustomersInformationType());
		//Добавляем цены
		$this->model_extension_odata->addSetDocumentPricesProduct($this->getSetDocumentPricesProduct(100,0));
		$this->model_extension_odata->addSetDocumentPricesProduct($this->getSetDocumentPricesProduct(100,200));
		$this->model_extension_odata->addSetDocumentPricesProduct($this->getSetDocumentPricesProduct(100,300));
		$this->model_extension_odata->addSetDocumentPricesProduct($this->getSetDocumentPricesProduct(100,400));

		//Типы цен
		$this->model_extension_odata->addSetDocumentPricesProductType($this->getSetDocumentPricesProductType());

	}

	/**
	 * возвращает каталог товаров
	 * @return array
	 */
	public function getCatalog()
	{
			return $this->odata->{'Catalog_Номенклатура/?$select=Description,Артикул,Ref_Key,Parent_Key,IsFolder&'}->get()->values();
	}

	/**
	 *Document_УстановкаЦенНоменклатуры_Товары
	 *table oc_document_set_prise_product
	 *@return array
	 */
	public function getSetDocumentPricesProduct($count,$skip)
	{
			$documents = $this->odata->{'Document_УстановкаЦенНоменклатуры?$select=Ref_Key,Date,Товары/Номенклатура_Key,Товары/ТипЦен_Key,Товары/Цена,Товары/Валюта_Key&$top='.$count.'&$skip='.$skip.''}->get()->values();

			$result = [];
			foreach ($documents as $document){

				foreach ($document['Товары'] as $item){
					$result[] =
						[
							'ref_key'=>$document['Ref_Key'],
							'product_кey'=>$item['Номенклатура_Key'],
							'type_price_key'=>$item['ТипЦен_Key'],
							'price'=> $item['Цена'] * $this->currency[$item['Валюта_Key']]['curs'],
							'date' => $document['Date']
						];
				}
			}

			return $result;
	}

	/**
	 *Document_УстановкаЦенНоменклатуры_ТипыЦен
	 *table oc_document_set_prise_product_type
	 * @return mixed array
	 */
	public function getSetDocumentPricesProductType()
	{
		 return $this->odata->{'Document_УстановкаЦенНоменклатуры_ТипыЦен?$select=Ref_Key,ТипЦен_Key&'}->get()->values();
	}

	/**
	 *Catalog_ТипыЦенНоменклатуры
	 *table oc_price_product_type
	 * @return mixed
	 */
	public function getPricesProductType()
	{
		return $this->odata->{'Catalog_ТипыЦенНоменклатуры?$select=Ref_Key,Description&'}->get()->values();
	}

	/**
	 *Catalog_ТипыЦенНоменклатуры
	 *table oc_price_product_type_by_customer
	 * @return mixed
	 */
	public function getPricesProductTypeByCustomer()
	{
		return  $this->odata->{'Document_УстановкаТиповЦенПоГруппамНоменклатурыДляПокупателей?$select=Ref_Key,Контрагент_Key'}->get()->values();
	}
	/**
	 *Catalog_ТипыЦенНоменклатуры
	 *table oc_price_product_type_price_group
	 * @return mixed
	 */
	public function getPricesProductTypeByCustomerPriceGroup()
	{
		return  $this->odata->{'Document_УстановкаТиповЦенПоГруппамНоменклатурыДляПокупателей_НоменклатурныеЦеновыеГруппы'}->get()->values();
	}


	/**
	 * возвращает цены
	 * @return array
	 */
	public function getPrises(){
		return $this->odata->{'InformationRegister_ЦеныНоменклатуры?$select=RecordSet/Номенклатура_Key,RecordSet/Цена,RecordSet/ТипЦен_Key&'}->get()->values();
	}


	/**
	 * возваращает ценовые группы
	 * @return array
	 */
	public function getPrisesCategory(){
		return $this->odata->{'Catalog_ЦеновыеГруппы?$select=Description,Ref_Key&'}->get()->values();
	}

	/**
	 * возвращает количество товара
	 * @return array
	 */
	public function getQuantyty(){
		$quantyty =  $this->odata->{'AccumulationRegister_ТоварыНаСкладах/Balance?&$select=КоличествоBalance,Номенклатура_Key&'}->get()->values();
		$result_quantity = [];
		foreach ($quantyty as $count) {
			$result_quantity[$count['Номенклатура_Key']] = ($count['КоличествоBalance'] > 0) ? $count['КоличествоBalance'] : 0;
		}
		return $result_quantity;

	}

	public function getCustomers()
	{

		$dataCustomers = $this->odata->{'Catalog_Контрагенты'}->get()->values();

		$сustomers = [];
		foreach ($dataCustomers as $customer){
			$сustomers[$customer['Ref_Key']] = new Customer($customer);
		}
			return $сustomers;

	}

	/**
	 *Иноформация пользователей
	 */
	public function getCustomerInformation()
	{
		$information = $this->odata->{'InformationRegister_%D0%9A%D0%BE%D0%BD%D1%82%D0%B0%D0%BA%D1%82%D0%BD%D0%B0%D1%8F%D0%98%D0%BD%D1%84%D0%BE%D1%80%D0%BC%D0%B0%D1%86%D0%B8%D1%8F'}->get()->values();
		$result = [];
		foreach ($information as $info){
			$result[] = new Information($info);
		}
		return $result;
	}
	/**
	 * получаем информацию пользователей типы
	 */
	public function getCustomersInformationType()
	{
		$information = $this->odata->{'Catalog_%D0%92%D0%B8%D0%B4%D1%8B%D0%9A%D0%BE%D0%BD%D1%82%D0%B0%D0%BA%D1%82%D0%BD%D0%BE%D0%B9%D0%98%D0%BD%D1%84%D0%BE%D1%80%D0%BC%D0%B0%D1%86%D0%B8%D0%B8'}->get()->values();
		$result = [];
		foreach ($information as $info){
			$result[] = new InformationType($info);
		}
		return $result;
	}

	/**
	 *сортирировка
	 */
	protected function sortProductsToCategory()
	{
		$this->model_extension_odata->sortCategories();
		$this->model_extension_odata->sortProducts();
	}

	/**
	 *обработка кастомеров
	 */
	protected  function sortCustomers($id)
	{
		$this->model_extension_odata->addProductToCustomer($id);
	}

	protected  function addActiveCustomers()
	{
		$this->model_extension_odata->addActiveCustomers();
	}

	/**
	 *загрузка валют
	 */
	protected function getCurrency(){
		 $this->currency = [];
		foreach ($this->odata->{'InformationRegister_КурсыВалют?&$top=2&$orderby=Period desc&'}->get()->values() as $currency){
			$this->currency[$currency['Валюта_Key']] = [
				'curs' => $currency['Курс'],
				'currency_id' => ($currency['Валюта_Key'] === '6d0f5f94-a771-11dd-96dc-000c6e46fcad') ? 3 : 2
			];
		}
		//костыль для рубля
		$this->currency['6d0f5f91-a771-11dd-96dc-000c6e46fcad']=[
			'curs' => 1,
			'currency_id' => 1,
		];
		//кослыль для какого то "у.е"
		$this->currency['4883732c-b0de-11e4-aff5-08606ed5802f']=[
			'curs' => 1,
			'currency_id' => 1,
		];
	}

}
